﻿namespace WindowsFormsApplication1
{
    partial class groupinfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.back = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            this.groupphoto = new System.Windows.Forms.Panel();
            this.rozklad = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // back
            // 
            this.back.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.back;
            this.back.Location = new System.Drawing.Point(896, 512);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(100, 100);
            this.back.TabIndex = 11;
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // exit
            // 
            this.exit.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.exit;
            this.exit.Location = new System.Drawing.Point(896, 618);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(100, 100);
            this.exit.TabIndex = 12;
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // groupphoto
            // 
            this.groupphoto.Location = new System.Drawing.Point(309, 154);
            this.groupphoto.Name = "groupphoto";
            this.groupphoto.Size = new System.Drawing.Size(366, 127);
            this.groupphoto.TabIndex = 13;
            // 
            // rozklad
            // 
            this.rozklad.Location = new System.Drawing.Point(285, 379);
            this.rozklad.Name = "rozklad";
            this.rozklad.Size = new System.Drawing.Size(420, 297);
            this.rozklad.TabIndex = 14;
            // 
            // groupinfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.grass;
            this.ClientSize = new System.Drawing.Size(1008, 730);
            this.Controls.Add(this.rozklad);
            this.Controls.Add(this.groupphoto);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.back);
            this.Name = "groupinfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "groupinfo";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Button exit;
        public System.Windows.Forms.Panel groupphoto;
        public System.Windows.Forms.Panel rozklad;
    }
}