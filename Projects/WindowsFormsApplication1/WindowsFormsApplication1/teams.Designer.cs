﻿namespace WindowsFormsApplication1
{
    partial class teams
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = global::WindowsFormsApplication1.Properties.Resources.b11;
            this.button1.Location = new System.Drawing.Point(195, 174);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 108);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.b2;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Location = new System.Drawing.Point(374, 178);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(108, 104);
            this.button2.TabIndex = 1;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = global::WindowsFormsApplication1.Properties.Resources.b3;
            this.button3.Location = new System.Drawing.Point(565, 178);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(108, 100);
            this.button3.TabIndex = 16;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Image = global::WindowsFormsApplication1.Properties.Resources.b4;
            this.button4.Location = new System.Drawing.Point(738, 172);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(111, 106);
            this.button4.TabIndex = 30;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Image = global::WindowsFormsApplication1.Properties.Resources.b5;
            this.button5.Location = new System.Drawing.Point(195, 312);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(117, 104);
            this.button5.TabIndex = 31;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Image = global::WindowsFormsApplication1.Properties.Resources.b6;
            this.button6.Location = new System.Drawing.Point(373, 312);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(109, 104);
            this.button6.TabIndex = 32;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.b7;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Location = new System.Drawing.Point(565, 310);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(126, 109);
            this.button7.TabIndex = 33;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Image = global::WindowsFormsApplication1.Properties.Resources.b8;
            this.button8.Location = new System.Drawing.Point(738, 310);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(117, 102);
            this.button8.TabIndex = 34;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Image = global::WindowsFormsApplication1.Properties.Resources.b9;
            this.button9.Location = new System.Drawing.Point(195, 445);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(121, 104);
            this.button9.TabIndex = 35;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button10
            // 
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Image = global::WindowsFormsApplication1.Properties.Resources.b10;
            this.button10.Location = new System.Drawing.Point(374, 443);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(119, 108);
            this.button10.TabIndex = 36;
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Image = global::WindowsFormsApplication1.Properties.Resources.b16;
            this.button11.Location = new System.Drawing.Point(740, 570);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(119, 107);
            this.button11.TabIndex = 37;
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button12
            // 
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Image = global::WindowsFormsApplication1.Properties.Resources.b12;
            this.button12.Location = new System.Drawing.Point(740, 439);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(115, 106);
            this.button12.TabIndex = 38;
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button13
            // 
            this.button13.FlatAppearance.BorderSize = 0;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Image = global::WindowsFormsApplication1.Properties.Resources.b13;
            this.button13.Location = new System.Drawing.Point(195, 573);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(121, 105);
            this.button13.TabIndex = 39;
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // button14
            // 
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Image = global::WindowsFormsApplication1.Properties.Resources.b14;
            this.button14.Location = new System.Drawing.Point(376, 571);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(111, 109);
            this.button14.TabIndex = 40;
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // button15
            // 
            this.button15.FlatAppearance.BorderSize = 0;
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.Image = global::WindowsFormsApplication1.Properties.Resources.b15;
            this.button15.Location = new System.Drawing.Point(572, 570);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(119, 106);
            this.button15.TabIndex = 41;
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // button16
            // 
            this.button16.FlatAppearance.BorderSize = 0;
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.Image = global::WindowsFormsApplication1.Properties.Resources.b111;
            this.button16.Location = new System.Drawing.Point(570, 443);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(118, 106);
            this.button16.TabIndex = 42;
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // exit
            // 
            this.exit.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.exit;
            this.exit.Location = new System.Drawing.Point(913, 657);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(100, 100);
            this.exit.TabIndex = 43;
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.button17_Click);
            // 
            // back
            // 
            this.back.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.back;
            this.back.Location = new System.Drawing.Point(913, 551);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(100, 100);
            this.back.TabIndex = 44;
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // teams
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.fon1;
            this.ClientSize = new System.Drawing.Size(1025, 769);
            this.Controls.Add(this.back);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "teams";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form2";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.Button back;



    }
}