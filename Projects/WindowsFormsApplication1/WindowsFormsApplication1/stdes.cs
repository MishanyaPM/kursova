﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class stdes : Form
    {
        public stdes()
        {
            InitializeComponent();
        }

        private void back_Click(object sender, EventArgs e)
        {
            stadium stadium = new stadium();
            stadium.Show();
            Hide();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
