﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class mainform : Form
    {
        public mainform()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            teams team = new teams();
            team.Show();
            Hide();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            stadium stadium = new stadium();
            stadium.Show();
            Hide();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            playoff playoff = new playoff();
            playoff.Show();
            Hide();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            group group = new group();
            group.Show();
            Hide();
        }

         }
}
