﻿namespace WindowsFormsApplication1
{
    partial class teamsdes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.teamphoto = new System.Windows.Forms.Panel();
            this.exit = new System.Windows.Forms.Button();
            this.back = new System.Windows.Forms.Button();
            this.teaminfo = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // teamphoto
            // 
            this.teamphoto.Location = new System.Drawing.Point(247, 85);
            this.teamphoto.Name = "teamphoto";
            this.teamphoto.Size = new System.Drawing.Size(500, 313);
            this.teamphoto.TabIndex = 0;
            // 
            // exit
            // 
            this.exit.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.exit;
            this.exit.Location = new System.Drawing.Point(913, 657);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(100, 100);
            this.exit.TabIndex = 5;
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.button6_Click);
            // 
            // back
            // 
            this.back.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.back;
            this.back.Location = new System.Drawing.Point(913, 551);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(100, 100);
            this.back.TabIndex = 6;
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.button1_Click);
            // 
            // teaminfo
            // 
            this.teaminfo.Location = new System.Drawing.Point(153, 476);
            this.teaminfo.Name = "teaminfo";
            this.teaminfo.Size = new System.Drawing.Size(693, 245);
            this.teaminfo.TabIndex = 7;
            // 
            // teamsdes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.grass;
            this.ClientSize = new System.Drawing.Size(1025, 769);
            this.Controls.Add(this.teaminfo);
            this.Controls.Add(this.back);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.teamphoto);
            this.Name = "teamsdes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "teamsdes";
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel teamphoto;
        private System.Windows.Forms.Button exit;
        private System.Windows.Forms.Button back;
        public System.Windows.Forms.Panel teaminfo;

    }
}