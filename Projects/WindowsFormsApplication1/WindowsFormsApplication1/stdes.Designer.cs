﻿namespace WindowsFormsApplication1
{
    partial class stdes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.back = new System.Windows.Forms.Button();
            this.exit = new System.Windows.Forms.Button();
            this.stadiumphoto = new System.Windows.Forms.Panel();
            this.stadinfo = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // back
            // 
            this.back.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.back;
            this.back.Location = new System.Drawing.Point(913, 551);
            this.back.Name = "back";
            this.back.Size = new System.Drawing.Size(100, 100);
            this.back.TabIndex = 7;
            this.back.UseVisualStyleBackColor = true;
            this.back.Click += new System.EventHandler(this.back_Click);
            // 
            // exit
            // 
            this.exit.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.exit;
            this.exit.Location = new System.Drawing.Point(913, 657);
            this.exit.Name = "exit";
            this.exit.Size = new System.Drawing.Size(100, 100);
            this.exit.TabIndex = 8;
            this.exit.UseVisualStyleBackColor = true;
            this.exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // stadiumphoto
            // 
            this.stadiumphoto.Location = new System.Drawing.Point(226, 62);
            this.stadiumphoto.Name = "stadiumphoto";
            this.stadiumphoto.Size = new System.Drawing.Size(580, 326);
            this.stadiumphoto.TabIndex = 9;
            // 
            // stadinfo
            // 
            this.stadinfo.Location = new System.Drawing.Point(217, 462);
            this.stadinfo.Name = "stadinfo";
            this.stadinfo.Size = new System.Drawing.Size(600, 217);
            this.stadinfo.TabIndex = 10;
            // 
            // stdes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::WindowsFormsApplication1.Properties.Resources.grass;
            this.ClientSize = new System.Drawing.Size(1025, 769);
            this.Controls.Add(this.stadinfo);
            this.Controls.Add(this.stadiumphoto);
            this.Controls.Add(this.exit);
            this.Controls.Add(this.back);
            this.Name = "stdes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "stdes";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button back;
        private System.Windows.Forms.Button exit;
        public System.Windows.Forms.Panel stadiumphoto;
        public System.Windows.Forms.Panel stadinfo;
    }
}