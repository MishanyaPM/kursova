﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class group : Form
    {
        public group()
        {
            InitializeComponent();
        }

        private void back_Click(object sender, EventArgs e)
        {
            mainform mainform = new mainform();
            mainform.Show();
            Hide();
        }

        private void exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            groupinfo groupinfo = new groupinfo();
            groupinfo.Show();
            Hide();
            groupinfo.groupphoto.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\Agroup.jpg");
            groupinfo.rozklad.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\Aroz.jpg");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            groupinfo groupinfo = new groupinfo();
            groupinfo.Show();
            Hide();
            groupinfo.groupphoto.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\Bgroup.jpg");
            groupinfo.rozklad.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\Broz.jpg");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            groupinfo groupinfo = new groupinfo();
            groupinfo.Show();
            Hide();
            groupinfo.groupphoto.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\Cgroup.jpg");
            groupinfo.rozklad.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\Croz.jpg");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            groupinfo groupinfo = new groupinfo();
            groupinfo.Show();
            Hide();
            groupinfo.groupphoto.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\Dgroup.jpg");
            groupinfo.rozklad.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\Droz.jpg");
        }
    }
}
