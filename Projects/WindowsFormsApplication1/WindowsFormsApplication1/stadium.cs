﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class stadium : Form
    {
        public stadium()
        {
            InitializeComponent();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            stdes stdes = new stdes();
            stdes.Show();
            Hide();
            stdes.stadiumphoto.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\varsava.jpg");
            stdes.stadinfo.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\warsinfo.jpg");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            stdes stdes = new stdes();
            stdes.Show();
            Hide();
            stdes.stadiumphoto.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\gdanstad.jpg");
            stdes.stadinfo.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\gdaninfo.jpg");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            stdes stdes = new stdes();
            stdes.Show();
            Hide();
            stdes.stadiumphoto.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\vrozlav.jpg");
            stdes.stadinfo.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\vrocinfo.jpg");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            stdes stdes = new stdes();
            stdes.Show();
            Hide();
            stdes.stadiumphoto.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\poznanstad.jpg");
            stdes.stadinfo.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\pozninfo.jpg");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            stdes stdes = new stdes();
            stdes.Show();
            Hide();
            stdes.stadiumphoto.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\kiev.jpg");
            stdes.stadinfo.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\kievinfo.jpg");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            stdes stdes = new stdes();
            stdes.Show();
            Hide();
            stdes.stadiumphoto.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\donbass.jpg");
            stdes.stadinfo.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\doneinfo.jpg");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            stdes stdes = new stdes();
            stdes.Show();
            Hide();
            stdes.stadiumphoto.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\kharkivstad.jpg");
            stdes.stadinfo.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\kharinfo.jpg");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            stdes stdes = new stdes();
            stdes.Show();
            Hide();
            stdes.stadiumphoto.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\lvivstad.jpg");
            stdes.stadinfo.BackgroundImage = Image.FromFile
   (System.Environment.GetFolderPath
   (System.Environment.SpecialFolder.Personal)
   + @"\kyrsova\lvivinfo.jpg");
        }

        private void back_Click(object sender, EventArgs e)
        {
            mainform mainform = new mainform();
            mainform.Show();
            Hide();
        }
    }
}
